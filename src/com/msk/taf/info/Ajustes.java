package com.msk.taf.info;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.MenuItem;
import android.widget.Toast;

import com.msk.taf.R;
import com.msk.taf.db.DBTAF;
import com.msk.taf.db.ExportaParaExcel;
import com.msk.taf.db.ImportaDeExcel;

public class Ajustes extends PreferenceActivity implements
		OnPreferenceClickListener {

	private Preference backup, restaura, apagatudo, versao, exportar, importar, sobre;
	private CheckBoxPreference idade;
	private PreferenceScreen prefs;
	private String chave, nrVersao, arquivo;
	private PackageInfo info;
	private String[][] dadosExportar;
	private String[] dadosImportar;
	private int qtTestes = 0;
	Cursor buscaTAF = null;

	DBTAF dbTestes = new DBTAF(this);
	ExportaParaExcel excel = new ExportaParaExcel();
	ImportaDeExcel planilha = new ImportaDeExcel();
	final int ESCOLHE_ARQUIVO = 1;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferencias);

		prefs = getPreferenceScreen();

		Iniciador();
		usarActionBar();
		BuscaValoresBD();

		backup.setOnPreferenceClickListener(this);
		restaura.setOnPreferenceClickListener(this);
		apagatudo.setOnPreferenceClickListener(this);
		exportar.setOnPreferenceClickListener(this);
		importar.setOnPreferenceClickListener(this);
		sobre.setOnPreferenceClickListener(this);
		idade.setOnPreferenceClickListener(this);
	}

	private void BuscaValoresBD() {
		// COLOCA VALORES DE DADOS NUMA MATRIZ
		dbTestes.open();

		buscaTAF = dbTestes.buscaTestes();
		qtTestes = buscaTAF.getCount();

		dadosExportar = new String[qtTestes][19];

		for (int j = 0; j < qtTestes + 1; j++) {
			for (int i = 0; i < 19; i++) {

				if ((j >= 0) && (j < qtTestes)) {
					if (buscaTAF.moveToFirst()) {
						buscaTAF.moveToPosition(j);

						dadosExportar[j][i] = buscaTAF.getString(i);
					}
				}
			}
		}
		dbTestes.close();

	}

	private void Iniciador() {
		// COLOCA OS ELEMENTOS NA TELA
		backup = (Preference) prefs.findPreference("backup");
		restaura = (Preference) prefs.findPreference("restaura");
		apagatudo = (Preference) prefs.findPreference("apagatudo");
		versao = (Preference) prefs.findPreference("versao");
		exportar = (Preference) prefs.findPreference("excel");
		importar = (Preference) prefs.findPreference("importar");
		sobre = (Preference) prefs.findPreference("desenvolvedor");
		idade = (CheckBoxPreference) prefs.findPreference("idade");

		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		nrVersao = info.versionName;

		versao.setSummary(getResources().getString(
				R.string.pref_descricao_versao, nrVersao));

		if (idade.isChecked()) {
			idade.setSummary(R.string.pref_descricao_idade);
		} else {
			idade.setSummary(R.string.pref_descricao_data_nasc);
		}
	}

	@Override
	public boolean onPreferenceClick(Preference itemPref) {

		chave = itemPref.getKey();

		if (chave.equals("backup")) {
			// CRIA UMA COPIA DO BANCO DE DADOS NO SD
			dbTestes.open();
			dbTestes.copiaBD();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_copia_bd), Toast.LENGTH_SHORT)
					.show();
			dbTestes.close();
		}
		if (chave.equals("restaura")) {
			// RESTAURA O BANCO DE DADOS
			dbTestes.open();
			dbTestes.restauraBD();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_restaura_bd), Toast.LENGTH_SHORT)
					.show();
			dbTestes.close();
		}
		if (chave.equals("apagatudo")) {
			// APAGA O BANCO DE DADOS
			dbTestes.open();
			dbTestes.deleteAll();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_exclusao_bd), Toast.LENGTH_SHORT)
					.show();
			dbTestes.close();
		}
		if (chave.equals("excel")) {
			// EXPORTA TESTES PARA EXCEL

			int erro = excel.ArquivoExcel(qtTestes, dadosExportar);

			if (erro == 0) {
				Toast.makeText(getApplicationContext(),
						getString(R.string.dica_exporta_excel),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (chave.equals("desenvolvedor")) {
			// INFORMACOES SOBRE O APLICATIVO
			Toast.makeText(getApplicationContext(),
					getString(R.string.info_app), Toast.LENGTH_SHORT).show();
		}
		if (chave.equals("idade")) {

			if (idade.isChecked()) {
				idade.setSummary(R.string.pref_descricao_idade);
				Toast.makeText(getApplicationContext(),
						getString(R.string.pref_descricao_idade),
						Toast.LENGTH_SHORT).show();
			} else {
				idade.setSummary(R.string.pref_descricao_data_nasc);
				Toast.makeText(getApplicationContext(),
						getString(R.string.pref_descricao_data_nasc),
						Toast.LENGTH_SHORT).show();
			}
		}

		if (chave.equals("importar")) {

			Intent chooseFile;
			Intent intent;
			chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
			chooseFile.setType("file/*");
			intent = Intent.createChooser(chooseFile, "Escolha o arquivo");
			startActivityForResult(intent, ESCOLHE_ARQUIVO);

		}

		setResult(RESULT_OK);

		return false;
	}

	private void SalvaNomeTestes() {

		int u = dadosImportar.length;
		
		for (int v = 0; v < u; v++) {
		
		dbTestes.open();
		
		dbTestes.salvaAvaliadoTeste(dadosImportar[v]);
		
		dbTestes.close();
			
		}
		
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ESCOLHE_ARQUIVO: {
			if (resultCode == RESULT_OK) {
				Uri uri = data.getData();
				arquivo = uri.getPath();
				
				dadosImportar = planilha.ArquivoImportado(arquivo);
				
				SalvaNomeTestes();
				
				Toast.makeText(getApplicationContext(),
						getString(R.string.dica_importa_excel),
						Toast.LENGTH_SHORT).show();
				
			}
		}
		}
	}

}
