package com.msk.taf;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TesteAvaliacaoFisica extends ActionBarActivity implements OnClickListener {
	
	private Button criaTeste, listaTestes;

	ActionBar actionBar;

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.inicio_taf);
		criaTeste = (Button) findViewById(R.id.btCriaTeste);
		listaTestes = (Button) findViewById(R.id.btListaTestes);

		usarActionBar();
		
		criaTeste.setOnClickListener(this);
		listaTestes.setOnClickListener(this);
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(false);
		
	}

	public boolean onCreateOptionsMenu(Menu paramMenu) {
		super.onCreateOptionsMenu(paramMenu);
		getMenuInflater().inflate(R.menu.menu_inicio, paramMenu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
		super.onOptionsItemSelected(paramMenuItem);
		switch (paramMenuItem.getItemId()) {
		case R.id.ajustes:
			startActivityForResult(new Intent("com.msk.taf.AJUSTES"), 0);
			break;
		case R.id.ajuda:
			startActivity(new Intent("com.msk.taf.AJUDA"));
			break;
		case R.id.sobre:
			startActivity(new Intent("com.msk.taf.SOBRE"));
			break;
		}
		return false;
	}

	@Override
	public void onClick(View botao) {

		switch (botao.getId()) {
		case R.id.btCriaTeste:
			startActivity(new Intent("com.msk.taf.CRIATESTE"));
			break;
		case R.id.btListaTestes:
			startActivity(new Intent("com.msk.taf.LISTATESTES"));
			break;
		}

	}
}