TesteAptidaoFisica
==================

Aplicativo para lançamento, conferência de valores em Tabela e obtenção de notas do Teste de Aptidão Física

Desenvolvido por Paulo E S Mesquita

Notas de versões desenvolvidas:

1.0 - Lançamento de idade e valors com cálculo da nota do TAF conforme Res. 114 do CBMMG;
1.1 - Armazenamento da nota em BD SQLite;
1.1.1 - Visualização de todas as notas e valores salvos;
1.1.2 - Apagar testes salvos;
1.2.0 - Novo visual;
1.2.1 - ViewPager para visualizar testes salvos;
1.2.2 - Preferências para fazer Backup no Cartão SD e recuperar;
1.2.3 - Excluir um teste específico;
1.2.4 - Editar e salvar um teste específico;
1.2.5 - Exportar resultados para outro formato de arquivo;

2.0 - Novo visual;
2.1 - Escolher entre data de nascimento ou idade;
2.2 - Formatação dos tempos dos testes;
2.3 - Formatação das notas parciais;
2.4 - Correção da navegação do aplicativo;
2.5 - Importar nomes dos avaliados;
2.5.1 - Formatar planilha com resultados;
